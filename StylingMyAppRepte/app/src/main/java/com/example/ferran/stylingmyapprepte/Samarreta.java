package com.example.ferran.stylingmyapprepte;

import android.os.Parcel;
import android.os.Parcelable;

public class Samarreta{

    private int codi;
    private String nom;
    private String color;
    private String talla;
    private double preu;
    private int img;

    public Samarreta(int codi, String nom, String color, String talla, double preu, int img){
        this.codi = codi;
        this.nom = nom;
        this.color = color;
        this.talla = talla;
        this.preu = preu;
        this.img = img;
    }

    public int getImg(){
        return this.img;
    }

    public double getPrice(){
        return this.preu;

    }



    public String getNom(){
        return this.nom;
    }

}
