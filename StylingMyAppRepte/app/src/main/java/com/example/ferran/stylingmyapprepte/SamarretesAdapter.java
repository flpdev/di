package com.example.ferran.stylingmyapprepte;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SamarretesAdapter extends ArrayAdapter {
    private Context context;
    private List<Samarreta> samarretes;

    public SamarretesAdapter(Context context, ArrayList<Samarreta> samarretes) {
        super(context, R.layout.samarreta_layout, samarretes);
        this.context = context;
        this.samarretes = samarretes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.samarreta_layout, null);

        ImageView img = (ImageView) item.findViewById(R.id.img);
        img.setImageResource(samarretes.get(position).getImg());

        //Button comprar = (Button) item.findViewById(R.id.btnComprar);

        TextView txtnom = (TextView) item.findViewById(R.id.txtNom);
        txtnom.setText(samarretes.get(position).getNom());

        TextView txtPreu = (TextView) item.findViewById(R.id.txtPrice);
        txtPreu.setText(String.valueOf(samarretes.get(position).getPrice()));

        return item;
    }

}
