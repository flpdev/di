package com.example.ferran.stylingmyapprepte;

        import android.app.Activity;
        import android.os.Bundle;
        import android.widget.ImageButton;
        import android.widget.ImageView;
        import android.widget.RadioButton;
        import android.widget.RadioGroup;
        import android.widget.TextView;

public class SecondActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        ImageView img = findViewById(R.id.img);
        img.setImageResource(R.drawable.green);

        ImageView imgBtn = findViewById(R.id.imgBtn);
        imgBtn.setImageResource(R.drawable.ic_share_black_24dp);
        TextView txt = findViewById(R.id.nomProducte);
        txt.setText(R.string.samarreta);

        RadioGroup rg = findViewById(R.id.radioGroup);
        RadioButton rb = findViewById(R.id.firstBtn);

        rg.check(rb.getId());
    }
}
