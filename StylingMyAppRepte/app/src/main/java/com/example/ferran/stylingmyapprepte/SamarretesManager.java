package com.example.ferran.stylingmyapprepte;

import java.util.ArrayList;
import java.util.List;

public class SamarretesManager {

    public static ArrayList<Samarreta> getSamarretes(){
        ArrayList<Samarreta> samarretes = new ArrayList<>();

        samarretes.add(new Samarreta(1, "Samarreta 1", "vermell", "L", 10.99, R.drawable.red));
        samarretes.add(new Samarreta(2, "Samarreta 2", "verd", "XL", 5.99, R.drawable.green));
        samarretes.add(new Samarreta(3, "Samarreta 3", "blau", "S", 19.99, R.drawable.blue));
        samarretes.add(new Samarreta(4, "Samarreta 4", "blanc", "M", 8.99, R.drawable.grey));
        samarretes.add(new Samarreta(5, "Samarreta 5", "gris", "XS", 14.99, R.drawable.red));
        samarretes.add(new Samarreta(6, "Samarreta 6", "verd", "L",9.99, R.drawable.green));
        samarretes.add(new Samarreta(7, "Samarreta 7", "blanca", "M", 9.99, R.drawable.blue));
        samarretes.add(new Samarreta(8, "Samarreta 8", "groc", "XL", 5.99, R.drawable.grey));
        samarretes.add(new Samarreta(9, "Samarreta 9", "blau", "XL", 19.99, R.drawable.red));
        samarretes.add(new Samarreta(10, "Samarreta 10", "vermell", "XL", 10.99, R.drawable.green));
        samarretes.add(new Samarreta(11, "Samarreta 11", "groc", "XL", 9.99, R.drawable.blue));
        samarretes.add(new Samarreta(12, "Samarreta 12", "gris", "XL", 3.99, R.drawable.grey));


        return samarretes;

    }


}
