package com.example.localtickets2.POJO;

import java.util.Comparator;

public class ConcertDateComparator implements Comparator<Concert> {

    @Override
    public int compare(Concert o1, Concert o2) {
        int num;
        num = o1.getRawDate().compareTo(o2.getRawDate());
        return num;
    }
}
