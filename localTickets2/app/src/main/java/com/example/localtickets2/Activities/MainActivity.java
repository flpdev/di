package com.example.localtickets2.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.localtickets2.Interfaces.ActionBar;
import com.example.localtickets2.R;

public class MainActivity extends AppCompatActivity implements ActionBar {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(MainActivity.this);
        setContentView(R.layout.activity_main);

        //VIEWS
        Button btnLogin = findViewById(R.id.btnLogin);
        ImageView imgLogo = findViewById(R.id.imgLogo);
        imgLogo.setImageDrawable(getDrawable(R.drawable.logo_white_big));
        Button btnRegister = findViewById(R.id.btnRegister);

        //Si es el primer cop que s'utilitza la app, mostra slider
        final String PREFS_NAME = "MyPrefsFile";

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        //Si el usuari ja esta loguejat, anar directament a la landing activity
        if(settings.getBoolean("loggedIn", false)){
            Intent intent = new Intent(MainActivity.this, Landing.class);
            startActivity(intent);
            finish();
        }

        if (settings.getBoolean("my_first_time", true)) {
            //the app is being launched for first time, do something

            Toast.makeText(this, "first time", Toast.LENGTH_LONG).show();

            settings.edit().putBoolean("my_first_time", false).apply();
        }

        //final comprovacio primer usa app

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Register.class);
                startActivity(intent);
            }
        });




    }
}
