package com.example.localtickets2.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.localtickets2.Interfaces.ActionBar;
import com.example.localtickets2.R;

public class SearchActivity extends AppCompatActivity implements ActionBar {

    LinearLayout btnConcerts;
    LinearLayout btnProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_search_activity);

        btnConcerts = findViewById(R.id.btnConcerts);
        btnProfile = findViewById(R.id.btnProfile);

        btnConcerts.setOnClickListener(v -> {
            Intent i = new Intent(SearchActivity.this, Landing.class);
            startActivity(i);
        });

        btnProfile.setOnClickListener(v -> {
            Intent i = new Intent(SearchActivity.this, Profile.class);
            startActivity(i);
        });
    }
}
