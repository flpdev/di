package com.example.localtickets2.POJO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {


    private static String dateFormat = "dd/MM/yyyy";

    private static  SimpleDateFormat sdf;

    public static Date String2Date(String date, String format) {

        setDateFormat(format);
        sdf =  new SimpleDateFormat(dateFormat);
        Date d = null;

        try {
            d = sdf.parse(date);

        }catch(ParseException ex) {
            System.out.println(ex.toString());
        }

        return d;
    }

    public static Date String2Date(String date) {


        Date d = null;
        sdf =  new SimpleDateFormat(dateFormat);
        try {
            d = sdf.parse(date);

        }catch(ParseException ex) {
            System.out.println(ex.toString());
        }

        return d;
    }

    public static String Date2String(Date date, String format){
        setDateFormat(format);
        sdf =  new SimpleDateFormat(dateFormat);
        String d;

        d = sdf.format(date);

        return d;
    }

    public static String Date2String(Date date){
        sdf =  new SimpleDateFormat(dateFormat);
        String d;

        d = sdf.format(date);

        return d;
    }


    public static void setDateFormat(String format){
        dateFormat = format;
    }

}
