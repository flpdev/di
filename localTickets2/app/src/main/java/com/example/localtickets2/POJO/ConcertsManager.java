package com.example.localtickets2.POJO;

import com.example.localtickets2.R;

import java.util.ArrayList;
import java.util.Random;

public class ConcertsManager {
    private static ArrayList<Concert> concerts = new ArrayList<>();

    public static ArrayList<Concert> getConcerts() {



        concerts.clear();
        concerts.add(new Concert("Foo fighters", DateParser.String2Date("16/4/2019"), "Bocanord", 9.99, R.drawable.foo, R.drawable.foo_album_cover, "Greatest hits"));
        concerts.add(new Concert("Aerosmith", DateParser.String2Date("17/4/2019"), "Fontana", 8.99, R.drawable.aero, R.drawable.aerosmith_album, "Dream on"));
        concerts.add(new Concert("Simple Plan", DateParser.String2Date("18/4/2019"), "Verdana", 2.99, R.drawable.sp, R.drawable.simple_plan_album, "Shut up"));
        concerts.add(new Concert("Green Day", DateParser.String2Date("19/4/2019"), "Bocanord", 19.99, R.drawable.green, R.drawable.green_day_album, "Dookie"));
        concerts.add(new Concert("Blink 182", DateParser.String2Date("20/4/2019"), "Fontana", 4.99, R.drawable.blink, R.drawable.blink_album, "Blink 182"));
        concerts.add(new Concert("Metallica", DateParser.String2Date("12/4/2019"), "Verdana", 3.99, R.drawable.metallica, R.drawable.metallica_album, "Master of puppets"));
        concerts.add(new Concert("Muse", DateParser.String2Date("13/4/2019"), "Bocanord", 7.99, R.drawable.muse, R.drawable.muse_album, "Simulation theory"));
        concerts.add(new Concert("Queen", DateParser.String2Date("14/4/2019"), "Fontana", 6.99, R.drawable.queen, R.drawable.queen_album, "Queen II"));
        concerts.add(new Concert("Sabaton", DateParser.String2Date("15/4/2019"), "Verdana", 5.99, R.drawable.sabaton, R.drawable.sabaton_album, "The last stand"));
        concerts.add(new Concert("Offspring", DateParser.String2Date("21/4/2019"), "VerdBocanordana", 7.99, R.drawable.off, R.drawable.offspring_album, "Smash"));

        return concerts;
    }

    public static ArrayList<Concert> getConcertsProfile(){

        concerts.clear();
        concerts.add(new Concert("Foo fighters", DateParser.String2Date("16/4/2019"), "Bocanord", 9.99, R.drawable.foo, R.drawable.foo_album_cover, "Greatest hits"));
        concerts.add(new Concert("Aerosmith", DateParser.String2Date("17/4/2019"), "Fontana", 8.99, R.drawable.aero, R.drawable.aerosmith_album, "Dream on"));
        concerts.add(new Concert("Simple Plan", DateParser.String2Date("18/4/2019"), "Verdana", 2.99, R.drawable.sp, R.drawable.simple_plan_album, "Shut up"));
        concerts.add(new Concert("Green Day", DateParser.String2Date("19/4/2019"), "Bocanord", 19.99, R.drawable.green, R.drawable.green_day_album, "Dookie"));

        return concerts;
    }
}
