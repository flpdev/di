package com.example.localtickets2.Activities;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.localtickets2.Interfaces.ActionBar;
import com.example.localtickets2.R;

public class Login extends AppCompatActivity implements ActionBar {
    boolean canClick;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(Login.this);
        setContentView(R.layout.activity_login);

        //Si es el primer cop que s'utilitza la app, mostra slider
        final String PREFS_NAME = "MyPrefsFile";

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        //Views
        Button btnLogin = findViewById(R.id.btnLogin);
        ImageButton imgHidePassword = findViewById(R.id.btnShowPassword);
        EditText txtBoxPassword = findViewById(R.id.txtBoxPassword);
        EditText txtBoxUser = findViewById(R.id.txtBoxUsername);
        TextView txtContact = findViewById(R.id.txtContacte);

        txtContact.setPaintFlags(txtContact.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        //canClick = checkValues(txtBoxPassword, txtBoxUser);
        canClick = true;

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Quan es fa login, guardar la variable a sharedPreferences

                if(canClick) {
                    //settings.edit().putBoolean("loggedIn", true).apply();
                    Intent intent = new Intent(Login.this, Landing.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Falta algun camp", Toast.LENGTH_LONG).show();
                }

            }
        });

        txtBoxPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                canClick = checkValues(txtBoxPassword, txtBoxUser);

            }

            @Override
            public void afterTextChanged(Editable s) {
                canClick = checkValues(txtBoxPassword, txtBoxUser);

            }
        });

        txtBoxUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                canClick = checkValues(txtBoxPassword, txtBoxUser);

            }

            @Override
            public void afterTextChanged(Editable s) {
                canClick = checkValues(txtBoxPassword, txtBoxUser);

            }
        });



        imgHidePassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    txtBoxPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                    txtBoxPassword.setSelection(txtBoxPassword.getText().length());
                    txtBoxPassword.setTypeface(Typeface.DEFAULT);
                }
                if(event.getAction() == MotionEvent.ACTION_UP){
                    txtBoxPassword.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    txtBoxPassword.setSelection(txtBoxPassword.getText().length());
                    txtBoxPassword.setTypeface(Typeface.DEFAULT);
                }
                return true;
            }
        });
    }

    public boolean checkValues(EditText p, EditText u){
        boolean value;
        String pass = p.getText().toString();
        String user = u.getText().toString();

        return !(pass.isEmpty() || user.isEmpty());
    }
}
