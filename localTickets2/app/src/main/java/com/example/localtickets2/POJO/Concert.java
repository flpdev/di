package com.example.localtickets2.POJO;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.Objects;

public class Concert implements Parcelable, Comparable<Concert> {
    private int img;
    private String nomGrup;
    private Date data;
    private String local;
    private double preu;
    private int albumImg;
    private String tour;

    public Concert(String nomGrup, Date data, String local, double preu, int img, int albumImg, String tour) {
        this.nomGrup = nomGrup;
        this.data = data;
        this.local = local;
        this.preu = preu;
        this.img = img;
        this.albumImg = albumImg;
        this.tour = tour;
    }

    public int getImg() {
        return img;
    }

    public String getNomGrup() {
        return nomGrup;
    }

    public String getData() {
        String data = DateParser.Date2String(this.data);
        return data;
    }

    public Date getRawDate(){
        return data;
    }

    public String getLocal() {
        return local;
    }

    public double getPreu() {
        return preu;
    }

    public int getAlbumImg(){
        return this.albumImg;
    }

    public String getTour(){
        return this.tour;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Concert concert = (Concert) o;
        return Objects.equals(getNomGrup(), concert.getNomGrup()) &&
                Objects.equals(getData(), concert.getData());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNomGrup(), getData());
    }

    @Override
    public int compareTo(Concert o) {
        int num;
        num = this.getRawDate().compareTo(o.getRawDate());
        return num;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.img);
        dest.writeString(this.nomGrup);
        dest.writeLong(this.data != null ? this.data.getTime() : -1);
        dest.writeString(this.local);
        dest.writeDouble(this.preu);
        dest.writeInt(this.albumImg);
        dest.writeString(this.tour);
    }

    protected Concert(Parcel in) {
        this.img = in.readInt();
        this.nomGrup = in.readString();
        long tmpData = in.readLong();
        this.data = tmpData == -1 ? null : new Date(tmpData);
        this.local = in.readString();
        this.preu = in.readDouble();
        this.albumImg = in.readInt();
        this.tour = in.readString();
    }

    public static final Creator<Concert> CREATOR = new Creator<Concert>() {
        @Override
        public Concert createFromParcel(Parcel source) {
            return new Concert(source);
        }

        @Override
        public Concert[] newArray(int size) {
            return new Concert[size];
        }
    };
}
