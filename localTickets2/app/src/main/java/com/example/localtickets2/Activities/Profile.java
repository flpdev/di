package com.example.localtickets2.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.localtickets2.Interfaces.ActionBar;
import com.example.localtickets2.POJO.Concert;
import com.example.localtickets2.POJO.ConcertDateComparator;
import com.example.localtickets2.POJO.ConcertsAdapter;
import com.example.localtickets2.POJO.ConcertsManager;
import com.example.localtickets2.R;

import java.util.ArrayList;
import java.util.Collections;

public class Profile extends AppCompatActivity implements ActionBar {

    LinearLayout btnConcerts;
    LinearLayout btnCerca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_profile);

        btnCerca = findViewById(R.id.btnSearch);
        btnConcerts = findViewById(R.id.btnConcerts);

        ArrayList<Concert> concerts = ConcertsManager.getConcertsProfile();
        Collections.sort(concerts, new ConcertDateComparator());
        ConcertsAdapter concertsAdapter = new ConcertsAdapter(concerts);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(concertsAdapter);


        btnCerca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile.this, SearchActivity.class);
                startActivity(i);
            }
        });

        btnConcerts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile.this, Landing.class);
                startActivity(i);
            }
        });

    }
}
