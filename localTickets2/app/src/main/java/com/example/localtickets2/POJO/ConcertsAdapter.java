package com.example.localtickets2.POJO;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.localtickets2.R;

import java.util.ArrayList;

public class ConcertsAdapter extends RecyclerView.Adapter<ConcertsAdapter.PlaceViewHolder> implements View.OnClickListener{


    private ArrayList<Concert> concerts;
    private View.OnClickListener listener;

    //Constructor
    public ConcertsAdapter(ArrayList<Concert> concerts){
        this.concerts = concerts;
    }

    @NonNull
    @Override
    public PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.concert_layout, viewGroup, false);
        itemView.setOnClickListener(this);
        PlaceViewHolder PlaceViewHolder = new PlaceViewHolder(itemView);
        return PlaceViewHolder;
    }

    @Override
    public void onClick(View view) {
        if(listener!= null){
            listener.onClick(view);
        }

    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceViewHolder placeViewHolder, int i) {
        placeViewHolder.bindConcert(concerts.get(i));
    }

    @Override
    public int getItemCount() {
        return concerts.size();
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView txtnomGrup;
        TextView txtData;


        public PlaceViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imgGrup);
            txtnomGrup = itemView.findViewById(R.id.nomGrup);
            txtData = itemView.findViewById(R.id.txtData);

        }

        public void bindConcert(Concert concert){
            imageView.setImageResource(concert.getImg());

            txtnomGrup.setText(concert.getNomGrup());
            txtData.setText(concert.getData());
        }
    }
}
