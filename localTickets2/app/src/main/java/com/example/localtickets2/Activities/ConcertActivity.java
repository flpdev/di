package com.example.localtickets2.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.localtickets2.Interfaces.ActionBar;
import com.example.localtickets2.POJO.Concert;
import com.example.localtickets2.R;

public class ConcertActivity extends AppCompatActivity implements ActionBar {

    private ImageView imgGrup;
    private ImageView imgAlbum;
    private Concert concert;
    private TextView txtNomGrup;
    private TextView txtData;
    private TextView txtPreu;
    private TextView txtLocalitzacio;
    private TextView txtEuros;
    private TextView txtTour;
    private Button btnComprar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_concert);

        concert = (Concert)getIntent().getExtras().get("concert");

        imgGrup = findViewById(R.id.imatgeGrup);
        imgAlbum = findViewById(R.id.imgAlbum);
        txtNomGrup = findViewById(R.id.txtNomGrup);
        txtData = findViewById(R.id.txtData);
        txtPreu = findViewById(R.id.txtPreu);
        txtLocalitzacio = findViewById(R.id.txtLocalitzacio);
        txtEuros = findViewById(R.id.txtEuros);
        txtTour =  findViewById(R.id.txtTour);
        btnComprar = findViewById(R.id.btnComprar);

        emplenarDades();


        btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ConcertActivity.this, CompraActivity.class);
                i.putExtra("concert", concert);
                startActivity(i);
            }
        });

    }

    public void emplenarDades(){
        imgGrup.setImageResource(concert.getImg());
        imgAlbum.setImageResource(concert.getAlbumImg());
        txtNomGrup.setText(concert.getNomGrup());
        txtData.setText(concert.getData());
        txtPreu.setText(String.valueOf(concert.getPreu()) + "€");
        txtEuros.setText(" entrada bàsica");
        txtLocalitzacio.setText(new StringBuilder().append("Carrer de ").append(concert.getLocal()).append(", Barcelona").toString());
        txtTour.setText(concert.getTour()+ " 2019 Tour");
    }
}
