package com.example.localtickets2.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.localtickets2.Interfaces.ActionBar;
import com.example.localtickets2.POJO.Concert;
import com.example.localtickets2.POJO.ConcertDateComparator;
import com.example.localtickets2.POJO.ConcertsAdapter;
import com.example.localtickets2.POJO.ConcertsManager;
import com.example.localtickets2.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Landing extends AppCompatActivity implements ActionBar {

    LinearLayout btnCerca;
    LinearLayout btnProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_landing);

        btnCerca = findViewById(R.id.btnSearch);
        btnProfile = findViewById(R.id.btnProfile);

        ArrayList<Concert> concerts = ConcertsManager.getConcerts();
        Collections.sort(concerts, new ConcertDateComparator());
        ConcertsAdapter concertsAdapter = new ConcertsAdapter(concerts);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(concertsAdapter);

        btnCerca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Landing.this, SearchActivity.class);
                startActivity(i);
            }
        });

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Landing.this, Profile.class);
                startActivity(i);
            }
        });

        concertsAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = recyclerView.getChildAdapterPosition(v);
                Concert concert =concerts.get(i);

                Intent intent = new Intent(Landing.this, ConcertActivity.class);
                intent.putExtra("concert", concert);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed(){
        finishAffinity();
        System.exit(0);
    }






}
