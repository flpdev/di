package com.example.localtickets2.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.localtickets2.Interfaces.ActionBar;
import com.example.localtickets2.POJO.Concert;
import com.example.localtickets2.R;

public class CompraActivity extends AppCompatActivity implements ActionBar {

    private TextView txtEntrada;
    private int numEntrades = 1;
    private TextView txtNumEntrades;
    private TextView txtPreuTotal;
    private Button btnAfegir;
    private Button btnRestar;
    private Button btnPagar;
    private EditText codi;
    private boolean descompte;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar.hideActionBar(this);
        setContentView(R.layout.activity_compra);

        Concert concert = (Concert) getIntent().getExtras().get("concert");

        txtEntrada = findViewById(R.id.txtEntrada);
        txtNumEntrades =  findViewById(R.id.txtNumEntrades);
        txtPreuTotal = findViewById(R.id.preuTotal);
        btnAfegir = findViewById(R.id.sumarEntrada);
        btnRestar = findViewById(R.id.restarEntrada);
        btnPagar = findViewById(R.id.btnPagar);
        codi = findViewById(R.id.codi);

        emplenarDades(concert);

        btnPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Compra realitzada correctament", Toast.LENGTH_LONG).show();
            }
        });

        btnAfegir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numEntrades++;
                emplenarDades(concert);
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(numEntrades>1){
                    numEntrades--;
                    emplenarDades(concert);
                }
            }
        });

        codi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                emplenarDades(concert);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emplenarDades(concert);
            }

            @Override
            public void afterTextChanged(Editable s) {
                emplenarDades(concert);
            }
        });
    }

    public void emplenarDades(Concert concert){
        txtEntrada.setText(concert.getNomGrup() + ", pista, "+concert.getPreu()+ "€");
        txtNumEntrades.setText(String.valueOf(numEntrades));

        if(!codi.getText().toString().equals("")){
            descompte = true;
        }else{
            descompte =false;
        }
        txtPreuTotal.setText("TOTAL:   " + refrescarPreu(concert, descompte) + " €");
    }

    public Double refrescarPreu(Concert concert, boolean descompte){
        Double preu;
        if(descompte){
            preu = concert.getPreu() * numEntrades;
            preu *= 0.9;
        }else{
            preu = concert.getPreu() * numEntrades;
        }
        return preu;
    }

}
